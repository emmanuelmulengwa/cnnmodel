import torch
from tqdm import tqdm
import numpy as np


training_data = np.load('training_data.npy', allow_pickle=True)
X = torch.Tensor([i[0] for i in tqdm(training_data)]).view(-1,28,28)
X = X/255.0
y = torch.Tensor([i[1] for i in tqdm(training_data)])

VAL_PCT = 0.1  # lets reserve 10% of our data for validation
val_size = int(len(X)*VAL_PCT)
print(val_size)

train_X = X[:-val_size]
train_y = y[:-val_size]

test_X = X[-val_size:]
test_y = y[-val_size:]
print(len(train_X), len(test_X))


